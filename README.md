# Indicaciones generales

## Iniciar proyecto

* Agregar proyecto .zip y descomprirlo en la carpeta de preferencia de su ordenador
* Debe tener instalado rvm con la versión de ruby: 2.5.1 y la versión rails: 5.2.3 (este se instala con bundle)
* Realizar bundle install para la instalación de gemas
* Crear base de datos: rake db:create
* Ejecutar migraciones: rake db:migrate
* Importar usuario administrador: rake db:seed
  Datos
    email: info@test.com
    pass: info@test.com
    rol: Administrador general
* Iniciar servidor: rails s