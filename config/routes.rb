Rails.application.routes.draw do
  devise_for :users,
    path_names: { sign_in: 'login', sign_out: 'logout', password: 'passwords' },
    controllers: { sessions: 'users/sessions', passwords: 'users/passwords' }
  root to: "test_logins#index", as: :root
  namespace :admin do
    root to: 'users#index'
    resources :users
  end
end
