# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if User.count == 0
  email = "info@test.com"
  user = User.create(email: email, password: email, password_confirmation: email, first_name: "Info", last_name: "Test", user_type: 1)
  puts "\n\n\n Se creó el usuario administrador #{email} \n\n\n"
end