module ApplicationHelper
  
  def bootstrap_class_for(flash_type)
    {
      success: 'alert-success',
      error: 'alert-danger',
      alert: 'alert-warning',
      notice: 'alert-primary'
    }[flash_type.to_sym] || flash_type.to_s
  end

  def link_to_edit url
    link_to url, class: "btn-edit-destroy", data: { turbolinks: false } do
      content_tag :p, "Editar"
    end
  end

  def link_to_destroy url
    link_to url, method: :delete, data: {confirm: '¿Está seguro que desea eliminar el registro?'}, class: "btn-edit-destroy" do
      content_tag :p, "Eliminar", class: "red-monza"
    end
  end

  def link_to_add url
    link_to url, class: "btn-link--action", data: { turbolinks: false } do
      content_tag(:p, "Agregar") + " " + content_tag(:i, "", class: "fas fa-plus-circle")
    end
  end
end
