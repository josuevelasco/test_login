class Admin::UsersController < Admin::ApplicationController

  before_action :validate_user_access!, only: [:new, :destroy]

  before_action :validate_user_edition!, only: [:edit]

  helper_method :url_form, :user, :places, :request_types, :user_filter_q

  def index
    @users = user_filter_q.result.order(created_at: :asc).paginate(:page => params[:page], :per_page => 2)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "Registro creado con éxito"
      redirect_to admin_root_path
    else
      render :new
    end
  end

  def update
    user.update_attributes(user_params)
    if user.save
      flash[:success] = "Registro actualizado con éxito"
      redirect_to admin_root_path
    else
      render :edit
    end
  end

  def destroy
    if user.destroy
      flash[:success] = "Registro eliminado con éxito"
    else
      flash[:error] = "No se pudo eliminar el registro"
    end
    redirect_to admin_users_path
  end

  private
    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :user_type, :active)
    end

    def user
      @user ||=
        case action_name
        when "edit", "update", "destroy"
          User.find(params[:id])
        end
    end

    def url_form
      @url_form ||=
        case action_name
        when "edit", "update"
          admin_user_path(user.id)
        when "create", "new"
          admin_users_path
        end
    end

    def user_filter_q
      @user_filter_q ||= User.ransack(params[:q]) 
    end

    def validate_user_access!
      redirect_to admin_root_path unless current_user.admin?
    end

    def validate_user_edition!
      unless current_user.admin?
        @user = User.find(params[:id]) rescue nil
        redirect_to admin_root_path unless current_user?(@user)
      end
    end

    def current_user? user
      user == current_user
    end
end