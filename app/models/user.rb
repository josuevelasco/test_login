class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  ROLE_ADMIN_SYSTEM = 1
  ROLE_AREA_SYSTEM = 2

  ROLES = {
    ROLE_ADMIN_SYSTEM => I18n.t("admin.user_type.superadmin"),
    ROLE_AREA_SYSTEM => I18n.t("admin.user_type.area"),
  }

  def role label=false
    label && ROLES.include?(read_attribute(:user_type)) ? ROLES[read_attribute(:user_type)] : read_attribute(:user_type)
  end

  def status
    ( read_attribute(:active) ? I18n.t('site.labels.active') : I18n.t('site.labels.inactive') )
  end

  def full_name
    [first_name, last_name].compact.join " "
  end

  def admin?
    ROLE_ADMIN_SYSTEM == role
  end
end
